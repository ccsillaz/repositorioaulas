public class ContaPoupanca extends Conta{

int dataAniversario;
double taxaJuros;

    public ContaPoupanca(String banco, int agencia, int numConta, double saldo, int dataAniversario, double taxaJuros) {
        super(banco, agencia, numConta, saldo);
        this.dataAniversario = dataAniversario;
        this.taxaJuros = taxaJuros;
    }

public double getSaldo(){
        return this.saldo*(this.taxaJuros/100+1);
}

    public String getTipoConta(){
        return "Poupanca";
    }

    public int getDataAniversario() {
        return dataAniversario;
    }

    public void setDataAniversario(int dataAniversario) {
        this.dataAniversario = dataAniversario;
    }

    public double getTaxaJuros() {
        return taxaJuros;
    }

    public void setTaxaJuros(double taxaJuros) {
        this.taxaJuros = taxaJuros;
    }

    public void depositar(double deposito){
        this.saldo=this.saldo+deposito;
    }

    @Override
    public double getValorImposto() {
        return 0;
    }

    public void sacar(double saque){

        if(this.saldo>=saque){
            this.saldo=this.saldo-saque;
            System.out.println("Saque efetuado com sucesso!");
        }else{
            System.out.println("Saldo Insuficiente!");
        }


    }

    @Override
    public String toString() {
        return "ContaPoupanca{" +
                "banco='" + banco + '\'' +
                ", agencia=" + agencia +
                ", numConta=" + numConta +
                ", saldo=" + saldo +
                ", dataAniversario=" + dataAniversario +
                ", taxaJuros=" + taxaJuros +
                '}';
    }
}
