public class ContaCorrente extends Conta implements Tributavel{

    double chequeEspecial;
    double impostoSaque=0;
    final double aliquota = 0.04;


    public ContaCorrente(String banco, int agencia, int numConta, double saldo,double chequeEspecial) {
        super(banco, agencia, numConta, saldo);
        this.chequeEspecial=chequeEspecial;

    }

    @Override
    public double getSaldo() {


        return this.saldo+this.chequeEspecial;
    }

    public String getTipoConta(){
        return "Corrente";
    }

    @Override
    public String toString() {
        return "ContaCorrente{" +
                "banco='" + banco + '\'' +
                ", agencia=" + agencia +
                ", numConta=" + numConta +
                ", saldo=" + saldo +
                ", chequeEspecial=" + chequeEspecial +
                '}';
    }

    public double getChequeEspecial() {
        return chequeEspecial;
    }

    public void setChequeEspecial(double chequeEspecial) {
        this.chequeEspecial = chequeEspecial;
    }

    public void depositar(double deposito){

        this.saldo=this.saldo+deposito;
    }

    public void sacar(double saque){
        if((this.saldo+this.chequeEspecial)>=(saque)){

            this.impostoSaque=this.impostoSaque+(saque*aliquota);
            this.saldo=this.saldo-saque;



            System.out.println("Saque efetuado com sucesso!");

        }else{
            System.out.println("Saldo Insuficiente!");
        }
    }

    @Override
    public double getValorImposto() {

return this.impostoSaque;



    }
}

