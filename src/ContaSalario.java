public class ContaSalario extends Conta implements Tributavel{

    final int LIMITE_SAQUE = 2;
    int numSaque=0;
    double impostoSaque=0;
    final double aliquotaSalario=0.02;

    public ContaSalario(String banco, int agencia, int numConta, double saldo) {
        super(banco, agencia, numConta, saldo);
    }

    public double getSaldo(){
        return this.saldo;
    }

    public void depositar(double deposito){
        System.out.println("Conta salário não permite depósito!");
    }



    public void sacar(double saque){
        if(this.saldo>=saque){

            if(numSaque<LIMITE_SAQUE){
                this.impostoSaque=this.impostoSaque+(saque*aliquotaSalario);
                this.saldo=this.saldo-saque;
                numSaque++;
                System.out.println("Saque efetuado com sucesso!");

            }else{
                System.out.println("Limite de Saques Excedido!");
            }
        }else{
            System.out.println("Saldo Insuficiente!");
        }
    }


    public String getTipoConta(){
        return "Salario";
    }


    @Override
    public double getValorImposto() {

        return this.impostoSaque;
    }
}
