import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Banco {

    String nomeDoBanco;
     //Hashtable<Cliente,Conta> contas = new Hashtable<Cliente,Conta>();
    double saldoTotal;
    List<Cliente> clientes = new ArrayList<Cliente>();




    public Banco(String nomeDoBanco){
        this.nomeDoBanco=nomeDoBanco;

    }

    //Metodo que cadastra um novo cliente no banco

    public void criarCliente(String nome){

        int count=0;

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nome)){

                count++;

            }

        }

        if(count==0){
            clientes.add(new Cliente(nome));
            System.out.println();
           // System.out.println("Cliente cadastrado com sucesso!");
        }else
            System.out.println("Cliente ja cadastrado!");

    }

    //Metodo que cria uma nova conta corrente, se o cliente colocado como parametro ainda não for
    //cadastrado no banco o metodo cria um novo cliente e a conta corrente vinculada a ele.

    public void criarContaCorrente(String nomeCliente,int agencia,int conta,double saldo,double cheque){

        int x=0;

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){

                c.addContaCorrente(this.nomeDoBanco,agencia,conta,saldo,cheque);
                x++;

            }

        }

        if(x!=0){
            System.out.println("Conta Corrente cadastrada com sucesso!");
        }else
            this.criarCliente(nomeCliente);

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){
                c.addContaCorrente(this.nomeDoBanco,agencia,conta,saldo,cheque);

            }

        }



    }

    public void criarContaPoupanca(String nomeCliente,int agencia,int conta,double saldo,int data,double juros){

        int x=0;

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){

                c.addContaPoupanca(this.nomeDoBanco,agencia,conta,saldo,data,juros);
                x++;

            }

        }

        if(x!=0){
            System.out.println("Conta Poupança cadastrada com sucesso!");
        }else
            this.criarCliente(nomeCliente);

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){
                c.addContaPoupanca(this.nomeDoBanco,agencia,conta,saldo,data,juros);

            }

        }



    }


    public void criarContaSalario(String nomeCliente,int agencia,int conta,double saldo){

        int x=0;

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){

                c.addContaSalario(this.nomeDoBanco,agencia,conta,saldo);
                x++;

            }

        }

        if(x!=0){
            System.out.println("Conta Salário cadastrada com sucesso!");
        }else
            this.criarCliente(nomeCliente);

        for(Cliente c:clientes){

            if(c.nomeCliente.equalsIgnoreCase(nomeCliente)){
                c.addContaSalario(this.nomeDoBanco,agencia,conta,saldo);

            }

        }



    }






}
