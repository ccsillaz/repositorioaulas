public abstract class Conta{

    String banco;
    protected int agencia;
    protected int numConta;
    protected double saldo;

    public Conta (String banco, int agencia, int numConta, double saldo) {
        this.banco = banco;
        this.agencia = agencia;
        this.numConta = numConta;
        this.saldo = saldo;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public abstract double getSaldo();

    public abstract void sacar(double saque);

    public abstract String getTipoConta();

    public abstract void depositar(double deposito);

    public abstract double getValorImposto();

    public void tranferirValor(Conta c, double valor){


        this.sacar(valor);
        c.depositar(valor);
    }

    @Override
    public String toString() {
        return "Conta{" +
                "banco='" + banco + '\'' +
                ", agencia=" + agencia +
                ", numConta=" + numConta +
                ", saldo=" + saldo +
                '}';
    }
}
