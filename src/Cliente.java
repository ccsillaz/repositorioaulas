public class Cliente {

    String nomeCliente;
    Conta contaCliente;


    public Cliente(String nomeCliente){
        this.nomeCliente=nomeCliente;
        //this.addContaCorrente(nomeCliente,0000,00000,0.00,0.00);

    }

    public void addContaCorrente(String banco, int agencia, int conta,double saldo,double chequeespecial){

        this.contaCliente=new ContaCorrente(banco,agencia,conta,saldo,chequeespecial);

    }
    public void addContaPoupanca(String banco, int agencia, int conta,double saldo,int dataaniversario,double taxajuros){

        this.contaCliente=new ContaPoupanca(banco,agencia,conta,saldo,dataaniversario,taxajuros);

    }
    public void addContaSalario(String banco, int agencia, int conta,double saldo){

        this.contaCliente=new ContaSalario(banco,agencia,conta,saldo);

    }

    public String getNomeCliente() {
        return this.nomeCliente;
    }

    public Conta getContaCliente() {

        return this.contaCliente;




    }
}
