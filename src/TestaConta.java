
import java.util.Scanner;

public class TestaConta {

    Banco banco;
    Scanner scan;

    public static void main(String[] args) {

        Banco banco = new Banco("Mentorama");
        int opcao;
        Scanner scan = new Scanner(System.in);
        String nome;
        int ag, ag2;
        int conta, conta2;
        int opcaoConta;
        int data;
        double juros;
        double saldo;
        double cheque;
        double valor;
        double soma = 0, soma2 = 0;


        do {

            System.out.println();
            System.out.println("************ Banco " + banco.nomeDoBanco + " *****************");
            System.out.println();
            System.out.println("1. Cadastrar Cliente");
            System.out.println("2. Cadastrar Conta");
            System.out.println("3. Sacar");
            System.out.println("4. Depositar");
            System.out.println("5. Tranferências");
            System.out.println("6. Mostrar Montante Disponível nas Contas");
            System.out.println("7. Listar Clientes");
            System.out.println("8. Sair");
            System.out.println();
            System.out.print("Digite a opção desejada: ");
            opcao = scan.nextInt();
            System.out.println();

            switch (opcao) {


                case 1:

                    System.out.print("Digite o nome do cliente: ");
                    banco.criarCliente(scan.next());
                    System.out.println("Cliente cadastrado com sucesso!");
                    System.out.println();
                    System.out.println("Digite uma letra para continuar...");
                    scan.next();

                    break;

                case 2:
                    System.out.println();
                    System.out.println("Escolha o tipo de conta: ");
                    System.out.println();
                    System.out.println("1. Conta Corrente");
                    System.out.println("2. Conta Poupança");
                    System.out.println("3. Conta Salário");
                    System.out.println();
                    System.out.print("Digite a opção desejada: ");
                    opcaoConta = scan.nextInt();
                    System.out.println();

                    switch (opcaoConta) {
                        case 1:
                            System.out.print("Digite o nome do cliente: ");
                            nome = scan.next();
                            System.out.print("Digite o número da agência: ");
                            ag = scan.nextInt();
                            System.out.print("Digite o número da conta: ");
                            conta = scan.nextInt();
                            System.out.print("Digite o saldo inicial: ");
                            saldo = scan.nextDouble();
                            System.out.print("Digite o valor do cheque especial: ");
                            cheque = scan.nextDouble();
                            int z = 0;

                            for (Cliente c : banco.clientes) {

                                if (c.getContaCliente() != null) {

                                    if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                                        z++;
                                    }
                                } else {

                                }
                            }

                            if (z == 0) {
                                banco.criarContaCorrente(nome, ag, conta, saldo, cheque);
                                System.out.println("Conta cadastrada com sucesso!");


                            } else if (z != 0) {
                                System.out.println();
                                System.out.println("Conta ja cadastrada anteriormente");
                            }
                            System.out.println();
                            System.out.println("Digite uma letra pra acontinuar...");
                            scan.next();
                            break;

                        case 2:

                            System.out.print("Digite o nome do cliente: ");
                            nome = scan.next();
                            System.out.print("Digite o número da agência: ");
                            ag = scan.nextInt();
                            System.out.print("Digite o número da conta: ");
                            conta = scan.nextInt();
                            System.out.print("Digite o saldo inicial: ");
                            saldo = scan.nextDouble();
                            System.out.print("Digite a data de aniversário: ");
                            data = scan.nextInt();
                            System.out.print("Digite a taxa de juros: ");
                            juros = scan.nextDouble();
                            int a = 0;

                            for (Cliente c : banco.clientes) {

                                if (c.getContaCliente() != null) {

                                    if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                                        a++;
                                    }
                                } else {

                                }


                            }

                            if (a == 0) {
                                banco.criarContaPoupanca(nome, ag, conta, saldo, data, juros);
                                System.out.println("Conta cadastrada com sucesso!");


                            } else if (a != 0) {
                                System.out.println();
                                System.out.println("Conta ja cadastrada anteriormente");
                                System.out.println(banco.clientes.toString());
                            }
                            System.out.println();
                            System.out.println("Digite uma letra para continuar...");
                            scan.next();

                            break;

                        case 3:

                            System.out.print("Digite o nome do cliente: ");
                            nome = scan.next();
                            System.out.print("Digite o número da agência: ");
                            ag = scan.nextInt();
                            System.out.print("Digite o número da conta: ");
                            conta = scan.nextInt();
                            System.out.print("Digite o saldo inicial: ");
                            saldo = scan.nextDouble();
                            int b = 0;

                            for (Cliente c : banco.clientes) {

                                if (c.getContaCliente() != null) {

                                    if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                                        b++;
                                    }
                                } else {

                                }

                            }

                            if (b == 0) {
                                banco.criarContaSalario(nome, ag, conta, saldo);
                                System.out.println("Conta cadastrada com sucesso!");


                            } else if (b != 0) {
                                System.out.println();
                                System.out.println("Conta ja cadastrada anteriormente!");
                            }

                            System.out.println();
                            System.out.println("Digite uma letra para continuar...");
                            scan.next();

                            break;
                    }
                    break;
                case 3:

                    System.out.print("Digite a agência: ");
                    ag = scan.nextInt();
                    System.out.print("Digite o número da conta: ");
                    conta = scan.nextInt();
                    System.out.print("Digite o valor que deseja sacar: ");
                    valor = scan.nextDouble();
                    int k = 0;

                    for (Cliente c : banco.clientes) {

                        if(c.getContaCliente()!=null) {
                            if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                                System.out.println();
                                c.contaCliente.sacar(valor);
                                k++;
                            }
                        }
                    }

                    if (k == 0) {
                        System.out.println("Conta ixexistente!");

                    }
                    System.out.println();
                    System.out.println("Digite uma letra para continuar...");
                    scan.next();

                    break;

                case 4:

                    System.out.print("Digite a agência: ");
                    ag = scan.nextInt();
                    System.out.print("Digite o número da conta: ");
                    conta = scan.nextInt();
                    System.out.print("Digite o valor que deseja depositar: ");
                    valor = scan.nextDouble();
                    System.out.println();
                    int sal = 0;
                    int x = 0;

                    for (Cliente c : banco.clientes) {
                        if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                            if (c.getContaCliente() instanceof ContaSalario) {
                                sal++;
                            }
                        }
                    }
                    for (Cliente c : banco.clientes) {
                        if(c.getContaCliente()!=null){
                        if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta)
                            x++;
                    }}

                    if (x == 0) {
                        System.out.println("Conta inexistente!");
                    }

                    for (Cliente c : banco.clientes) {
                        if(c.getContaCliente()!=null){
                        if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta && sal == 0) {
                            c.getContaCliente().depositar(valor);
                            System.out.println("Depósito efetuado com sucesso!");
                        } else if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta && sal != 0) {
                            System.out.println("Conta salário não permite depósito!");
                        } else {
                        }
                    }}


                    System.out.println();
                    System.out.println("Digite uma letra para continuar...");
                    scan.next();


                    break;

                case 5:

                    System.out.print("Digite a agência de origem: ");
                    ag = scan.nextInt();
                    System.out.print("Digite a conta de origem: ");
                    conta = scan.nextInt();
                    System.out.print("Digite a agência de destino: ");
                    ag2 = scan.nextInt();
                    System.out.print("Digite a conta de destino: ");
                    conta2 = scan.nextInt();
                    System.out.print("Digite o valor a ser transferido: ");
                    valor = scan.nextDouble();
                    int s = 0;
                    int c1 = 0;
                    int c2 = 0;
                    Conta a = null, b = null;


                    for (Cliente c : banco.clientes) {

                        if (c.getContaCliente() != null) {


                            if (c.getContaCliente().getAgencia() == ag && c.getContaCliente().getNumConta() == conta) {
                                c1++;
                                a = c.getContaCliente();

                            } else if (c.getContaCliente().getAgencia() == ag2 && c.getContaCliente().getNumConta() == conta2) {
                                c2++;
                                b = c.getContaCliente();
                            } else if (c.getContaCliente().getAgencia() == ag2 && c.getContaCliente().getNumConta() == conta2 && c.getContaCliente() instanceof ContaSalario) {
                                s++;
                            } else {

                            }
                        }else{}
                    }





                    if(c1==0&&c2==0){
                        System.out.println("Conta de origem e destino inexistentes!");
                    }else if(c1!=0&&c2==0){
                        System.out.println("Conta de destino inexistente!");
                    }else if(c1==0&&c2!=0){
                        System.out.println("Conta de origem inexistente!");
                    }else if(s!=0){
                        System.out.println("Conta salário não permite receber transferência!");
                }else if(c1!=0&&c2!=0&&s==0){

                            a.tranferirValor(b,valor);
                            System.out.println();
                            System.out.println("Tranferência realizada com sucesso!");
                            System.out.println();
                            System.out.println("Digite uma letra para continuar...");
                            scan.next();
                        }else{

                    }

                    break;

                case 6:

                    soma = 0;
                    for (Cliente c : banco.clientes) {

                        if (c.getContaCliente() != null) {

                            soma = soma + c.getContaCliente().getSaldo();
                        } else {

                        }
                    }
                    System.out.printf("Montante disponível nas contas: %.2f\n", soma);
                    System.out.println();
                    System.out.println("Digite uma letra para continuar...");
                    scan.next();

                    break;

                case 7:

                    System.out.format("%15s\t%10s\t%10s%13s\t\t%15s\t\t%15s","Nome","Agência","Conta","Saldo","Tipo de Conta","Impostos a Pagar");
                    System.out.println("\n-------------------------------------------------------------------------------------------------------");

                    for (Cliente c : banco.clientes) {

                        if (c.getContaCliente() != null) {

                            System.out.format("%15s\t%10d\t%10d\t%10.2f\t\t%10s\t\t%10.2f\n",c.getNomeCliente()  ,
                                    c.getContaCliente().getAgencia() ,
                                    c.getContaCliente().getNumConta(),
                                    c.getContaCliente().getSaldo(),
                                    c.getContaCliente().getTipoConta(),
                                    c.getContaCliente().getValorImposto());
                           // System.out.println(c.getContaCliente().getValorImposto());

                        } else {

                            System.out.format("%15s\t%10s\t%10s\t%10.2f\t\t%10s\n",c.getNomeCliente(),"----","----",0.0,"----");

                        }
                    }

                    System.out.println();
                    System.out.println("Digite uma letra para continuar...");
                    scan.next();
                    break;

                default:

                    if (opcao != 8) {
                        System.out.println();
                        System.out.println("Opção inválida!");
                        System.out.println("Digite uma letra para continuar...");
                        scan.next();
                    }
            }
        } while (opcao != 8);

        System.out.println("Fechando o programa...");

    }


}
